import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {
  @Output() rewriteStarted = new EventEmitter<void>();
  @Output() retrieveStarted = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  rewrite() {
    this.rewriteStarted.emit();
  }

  retrieve() {
    this.retrieveStarted.emit();
  }

}
