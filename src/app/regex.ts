/**/

export type RegEx = {
  expression: string,
  instead: string,
  isRewrite: boolean,
  factors: string[]
}

