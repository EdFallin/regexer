/**/

import {Component, ViewChild} from '@angular/core';

// Colleague types used in Mediator implementation.
import {ActionComponent} from './action/action.component';
import {RegexesComponent} from './regexes/regexes.component';
import {ResultComponent} from './result/result.component';
import {TextComponent} from './text/text.component';
import {RegEx} from './regex';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /* In addition to being the app's host, this class is used as a Mediator.
     Colleagues are ViewChildren, and events are wired in the template (HTML). */

  title = 'Regexer';

  // Declared this way, these members are set automatically,
  // and with static of true, are available at construction.
  @ViewChild(ActionComponent, {static: true}) actioner: ActionComponent;
  @ViewChild(RegexesComponent, {static: true}) regexer: RegexesComponent;
  @ViewChild(ResultComponent, {static: true}) resulter: ResultComponent;
  @ViewChild(TextComponent, {static: true}) texter: TextComponent;

  constructor() {
  }

  atRetrieveClick() {
    // Getting contents to work with.
    let regexes = this.regexer.nows;
    let text = this.texter.text;
    let results = '';

    // Only one expression is normal,
    // but more than one is possible.
    for (let regex of regexes) {
      // Actually retrieving.
      let expression = new RegExp(regex.expression, 'g');
      let matches = text.match(expression);

      // When an expression isn't matched, this happens.
      if (matches === null) {
        continue;
      }

      // Each match is listed on its own row.
      for (let match of matches) {
        results += (match + '\r\n');
      }
    }

    // To UI element.
    this.resulter.results = results;
  }

  atRewriteClick() {
    // Getting contents to work with.
    let regexes = this.regexer.nows;
    let text = this.texter.text;

    // Regex types might be mixed together.
    regexes = regexes.filter(x => x.isRewrite);

    console.log(`cruft : regexes:`, regexes);

    // Actually rewriting, in order.
    for (let regex of regexes) {
      let expression = new RegExp(regex.expression, 'g');
      text = text.replace(expression, regex.instead);
    }

    // To UI element.
    this.resulter.results = text;
  }
}
