import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RegexesComponent } from './regexes/regexes.component';
import { ActionComponent } from './action/action.component';
import { TextComponent } from './text/text.component';
import { ResultComponent } from './result/result.component';
import { RegexComponent } from './regex/regex.component';

@NgModule({
  declarations: [
    AppComponent,
    RegexesComponent,
    ActionComponent,
    TextComponent,
    ResultComponent,
    RegexComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
