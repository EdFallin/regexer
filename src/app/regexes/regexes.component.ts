/**/

import {Component, OnInit} from '@angular/core';
import {RegEx} from '../regex';

@Component({
  selector: 'app-regexes',
  templateUrl: './regexes.component.html',
  styleUrls: ['./regexes.component.css']
})
export class RegexesComponent implements OnInit {
  nows: RegEx[] = [];
  laters: RegEx[] = [];
  regexes: RegEx[] = [];

  prompt = 'Type a new regex here.';
  instead = 'Type a new replacement here.';

  constructor() {
    this.initSampleRegexes();
  }

  ngOnInit(): void {
  }

  initSampleRegexes() {
    // All that are stored.
    this.regexes.push({expression: 'laters-to-use-now', instead: '', isRewrite: false, factors: []});
    this.regexes.push({expression: 'laters-to-apply', instead: '', isRewrite: true, factors: []});
    this.regexes.push({expression: 'example-laters', instead: '', isRewrite: false, factors: []});
    this.regexes.push({expression: 'another-example', instead: '', isRewrite: true, factors: []});

    // Split into ones to use now, and ones ready for use.
    this.nows = [this.regexes[0], this.regexes[1]];
    this.laters = [this.regexes[2], this.regexes[3]];
  }

  whenRegexDragStarted(raised: DragEvent) {
    /* You don't preventDefault() here;
       the default is what you want. */

    let draggee = raised.currentTarget as HTMLElement;
    let id = draggee.id;

    raised.dataTransfer.setData('text/plain', id);
    raised.dataTransfer.dropEffect = 'move';
  }

  whenRegexDraggedOver(raised: DragEvent) {
    raised.preventDefault();

    let target = raised.currentTarget as HTMLElement;
    target.classList.add('over');
  }

  whenRegexDraggedOff(raised: Event) {
    raised.preventDefault();

    let target = raised.currentTarget as HTMLElement;
    target.classList.remove('over');
  }

  whenRegexDropped(raised: DragEvent) {
    raised.preventDefault();
    raised.stopPropagation();

    // Identifying what is being dragged.
    let id = raised.dataTransfer.getData('text/plain');
    let from = this.groupAndIndexFromId(id);

    // Identifying where it is being dragged to.
    let at = raised.currentTarget as HTMLElement;
    at.classList.remove('over');
    let to = this.groupAndIndexFromId(at.id);

    // The actual RegEx being moved.
    let across = from.group[from.index];

    // When contents of the groups are changed,
    // the displayed RegExes are changed, too.
    from.group.splice(from.index, 1);
    to.group.splice(to.index, 0, across);
  }

  whenIsRewriteClicked(raised: Event) {
    // Getting the identity of the regex with the checkbox.
    let target = raised.target as HTMLInputElement;
    let parent = target.parentNode as HTMLElement;
    let ancestor = parent.parentNode as HTMLElement;

    // Getting the regex itself.
    let { group, index } = this.groupAndIndexFromId(ancestor.id);
    let regex = group[index];

    // Changing the linked value.
    regex.isRewrite = target.checked;
  }

  idFromGroupAndIndex(group: any, at: number) /* passed */ {
    return `${group}-${at}`;
  }

  groupAndIndexFromId(id: string) {
    let source: RegEx[];

    let isNow = /^now/.test(id);
    let isLater = /^later/.test(id);

    if (isNow) {
      source = this.nows;
    }

    if (isLater) {
      source = this.laters;
    }

    let index = Number(id.match(/\d+/));

    let isNew = /new$/.test(id);

    if (isNew) {
      index = source.length;
    }

    return {
      group: source,
      index: index
    };
  }

  addRegexToNows(raised: Event) {
    this.addRegexToGroup(raised, this.nows);
  }

  addRegexToLaters(raised: Event) {
    this.addRegexToGroup(raised, this.laters);
  }

  addRegexToGroup(raised: Event, group: RegEx[]) {
    // Getting the sending empty regex.
    let target = raised.target as HTMLElement;
    let regex = target.parentNode as HTMLElement;

    // Getting regex content elements and their contents.
    let expressionEl = regex.querySelector("#Expression") as HTMLInputElement;
    let expression = expressionEl.value;
    let insteadEl = regex.querySelector("#Instead") as HTMLInputElement;
    let instead = insteadEl.value;
    let isRewriteEl = regex.querySelector("#IsRewrite") as HTMLInputElement;
    let isRewrite = isRewriteEl.checked;

    // Actually adding a new regex using the contents.
    let toAdd = { expression, instead, isRewrite, factors: [] };
    group.push(toAdd);

    // Also adding the regex to the general store of them.
    this.regexes.push(toAdd);

    // Emptying the sending regex for use again later.
    expressionEl.value = null;
    insteadEl.value = null;
    isRewriteEl.checked = false;
  }

  dumpRegexFromNows(raised: Event) {
    this.dumpRegexFromGroup(raised, this.nows);
  }

  dumpRegexFromLaters(raised: Event) {
    this.dumpRegexFromGroup(raised, this.laters);
  }

  dumpRegexFromGroup(raised: Event, group: RegEx[]) {
    // Getting the index of the regex to splice out.
    let target = raised.target as HTMLElement;
    let parent = target.parentNode as HTMLElement;
    let { index } = this.groupAndIndexFromId(parent.id);

    // Actually splicing out the regex.
    group.splice(index, 1);
  }
}
