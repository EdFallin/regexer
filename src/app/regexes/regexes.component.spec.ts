import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RegexesComponent} from './regexes.component';

describe('RegexesComponent', () => {
  let component: RegexesComponent;
  let fixture: ComponentFixture<RegexesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegexesComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegexesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the right text from idFromGroupAndIndex()',
    () => {
      let expected = 'laters-3';
      let actual = component.idFromGroupAndIndex('laters', 3);
      expect(actual).toBe(expected);
    });
});
